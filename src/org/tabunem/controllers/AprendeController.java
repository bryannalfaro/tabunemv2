/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar lo que ocurre en la ventana aprende
Ultima modificacion: 9/11/2019

 */
package org.tabunem.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Especialista;
import org.tabunem.models.Pregunta;

import java.awt.*;

import org.tabunem.models.Respuesta;
import org.tabunem.models.Usuario;
import org.tabunem.system.Main;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.ResourceBundle;

public class AprendeController implements Initializable {

    private Respuesta respuesta;
    private Especialista especialista;
    ArrayList<Respuesta> respuestas = new ArrayList<Respuesta>();
    ArrayList<Especialista> especialistas = new ArrayList<Especialista>();
    ObservableList<Pregunta> preguntas = FXCollections.observableArrayList();
    ObservableList<String> comentarios = FXCollections.observableArrayList();

    @FXML TableView preguntasTable;
    @FXML TableColumn preguntaColumn;
    @FXML TableColumn votosColumn;
    @FXML Label especialistaLabel;
    @FXML Label respuestaLabel;
    @FXML Text explicacionText;
    @FXML TableView comentariosTable;
    @FXML TableColumn comentarioColumn;

    dbConnection conexion = new dbConnection(); //Conexion a base de datos

    public AprendeController() throws Exception{
        ArrayList<Class<?>> tables = new ArrayList<Class<?>>();
        tables.add(Respuesta.class);
        tables.add(Pregunta.class);
        ResultSet rs = conexion.selectJoin(tables);
        while(rs.next()){
            respuestas.add(new Respuesta(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getInt(5)));
            preguntas.add(new Pregunta(rs.getInt(6),rs.getString(7),rs.getString(8),rs.getInt(9),rs.getInt(10),rs.getInt(11)));
        }
        rs = conexion.selectAll(Especialista.class);
        while (rs.next()) {
            ResultSet rs2 = conexion.selectById(Usuario.class, rs.getInt(1));
            while (rs2.next()) {
                especialistas.add(new Especialista(rs.getInt("id"),rs2.getString("nombre"),rs2.getInt("edad"),rs2.getString("userName"),"pass",rs2.getString("acercaDeMi"),rs.getString("especialidad"),rs.getInt("respondidas")));
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        preguntasTable.setItems(preguntas);
        preguntaColumn.setCellValueFactory(new PropertyValueFactory<Pregunta, String>("descripcion"));
        votosColumn.setCellValueFactory(new PropertyValueFactory<Pregunta, Integer>("votaciones"));
        //temp
        votosColumn.setVisible(false);
        comentarioColumn.setVisible(false);
        comentariosTable.setVisible(false);
    }

    public void seleccionar() {
        if (preguntasTable.getSelectionModel().getSelectedIndex() > -1) {
            respuesta = getRespuesta(((Pregunta)preguntasTable.getSelectionModel().getSelectedItem()).getId());
            especialista = getEspecialista(respuesta.getIdUsuario());
            especialistaLabel.setText(especialista.getNombre() + " - especialidad: " + especialista.getEspecialidad());
            respuestaLabel.setText("Respuesta: " + respuesta.getRespuesta());
            explicacionText.setText("Explicacion: " + respuesta.getExplicacion());
        }
    }

    private Respuesta getRespuesta(int id) {
        Respuesta res = null;
        for (Respuesta respuesta: respuestas) {
            if (respuesta.getIdPregunta() == id) {
                res = respuesta;
            }
        }
        return res;
    }

    private Especialista getEspecialista(int id) {
        Especialista esp = null;
        for (Especialista especialista: especialistas) {
            if (especialista.getId() == id) {
                esp = especialista;
            }
        }
        return esp;
    }

    //Metodo para regresar a la ventana principal al recibir el ActionEvent
    public void regresar(ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Principal.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }

}
