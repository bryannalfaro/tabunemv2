/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar la ventana de quiz
Ultima modificacion: 25/09/2019

 */
package org.tabunem.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Pregunta;
import org.tabunem.models.Quiz;
import org.tabunem.models.Respuesta;
import org.tabunem.system.Main;

//import java.awt.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.ResourceBundle;


public class QuizController implements Initializable {

    @FXML
    private Label preguntaNo;
    @FXML
    private Label correct;
    @FXML
    private Label question;
    @FXML
    private RadioButton option1;
    @FXML
    private RadioButton option2;
    @FXML
    private RadioButton option3;
    @FXML
    private RadioButton option4;
    @FXML
    private Label explain;
    @FXML
    private Button confirm;
    @FXML
    private Button next;
    @FXML
    private Label win;
    @FXML
    private Label winBuenas;
    @FXML
    private Label winBuenas1;
    @FXML
    private Label winMalas;
    @FXML
    private Label winMalas1;

    private dbConnection conexion = new dbConnection();
    private ArrayList<Respuesta> respuestas = new ArrayList<Respuesta>();
    private ArrayList<Pregunta> preguntas = new ArrayList<Pregunta>();
    private int cont = 0;
    private String optionSelected;
    private String[] options = new String[4];

    //POR EL MOMENTO
    private int contBuenas = 0;
    private int contMalas = 0;
    //

    public QuizController() throws Exception{
        ArrayList<Class<?>> values = new ArrayList<Class<?>>();
        // Se agregan las tablas con las que se quiere hacer join
        //tomando como tabla principal Respuesta
        values.add(Respuesta.class);
        values.add(Pregunta.class);
        // Ordena por RAND() para obtener registros random y LIMIT 10 para obtener solo 10
        ResultSet rs = conexion.selectJoinOrdered(values, "RAND()", "LIMIT 10");
        while(rs.next()){
            // Agrega las respuestas y preguntas obtenidas individualmente a sus listas
            Respuesta respuesta = new Respuesta(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5));
            Pregunta pregunta = new Pregunta(rs.getInt(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getInt(10), rs.getInt(11));
            respuestas.add(respuesta);
            preguntas.add(pregunta);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // ingresa la descripcion de la primera pregunta
        question.setText(preguntas.get(cont).getDescripcion());
        // splitea las opciones
        options = preguntas.get(cont).getOpciones().split("/");
        //TODO: Solucion por ahora para tener los RadioButton de la cantidad de opciones que son
        if (options.length == 2) {
            option1.setText(options[0]);
            option2.setText(options[1]);
            option3.setVisible(false);
            option4.setVisible(false);
        } if (options.length == 3) {
            option1.setText(options[0]);
            option2.setText(options[1]);
            option3.setText(options[2]);
            option4.setVisible(false);
        } if (options.length == 4) {
            option1.setText(options[0]);
            option2.setText(options[1]);
            option3.setText(options[2]);
            option4.setText(options[3]);
        }
        confirm.setDisable(true);
    }

    //Metodo para regresar a la ventana principal al recibir el ActionEvent
    public void changeScreenButtonPushed(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Principal.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }

    @FXML
    private void confirm(ActionEvent actionEvent) {
        // comprueba que la pregunta seleccionada coincida con la correcta
        if (respuestas.get(cont).getRespuesta().equals(optionSelected)) {
            correct.setText("Correcto!");
            correct.setTextFill(Paint.valueOf("#00FF00"));
            contBuenas++;
        } else {
            correct.setText("Incorrecto!");
            correct.setTextFill(Paint.valueOf("#ff0d00"));
            contMalas++;
        }
        //muestra la explicacion y cambia los botones
        explain.setText(respuestas.get(cont).getExplicacion());
        confirm.setVisible(false);
        next.setVisible(true);
        option1.setDisable(true);
        option2.setDisable(true);
        option3.setDisable(true);
        option4.setDisable(true);
        cont++;
    }

    public void next(ActionEvent actionEvent) {
        // liempia los elementos principales
        option1.setDisable(false);
        option2.setDisable(false);
        option3.setDisable(false);
        option4.setDisable(false);
        correct.setText("");
        explain.setText("");
        next.setVisible(false);
        confirm.setVisible(true);
        confirm.setDisable(true);
        // comprueba si el quiz ya termino
        if (cont == 10) {
            // muestra la pantalla final con resultados
            finished();
        } else {
            // cambia a la te pregunta y limpia los radios
            chargeNewQuestion();
            clearRadios();
            preguntaNo.setText("Responda la pregunta " + (cont + 1));
        }
    }

    private void chargeNewQuestion() {
        // setea los datos de la nueva pregunta
        question.setText(preguntas.get(cont).getDescripcion());
        options = preguntas.get(cont).getOpciones().split("/");
        //TODO: Solucion por ahora
        option1.setVisible(true);
        option2.setVisible(true);
        option3.setVisible(true);
        option4.setVisible(true);
        if (options.length == 2) {
            option1.setText(options[0]);
            option2.setText(options[1]);
            option3.setVisible(false);
            option4.setVisible(false);
        } if (options.length == 3) {
            option1.setText(options[0]);
            option2.setText(options[1]);
            option3.setText(options[2]);
            option4.setVisible(false);
        } if (options.length == 4) {
            option1.setText(options[0]);
            option2.setText(options[1]);
            option3.setText(options[2]);
            option4.setText(options[3]);
        }
    }

    @FXML
    private void radioSelected(ActionEvent actionEvent) {
        // comprueba que radioButton esta seleccionado y obtiene la opcion segun el seleccionado
        if (option1.isSelected()) {
            optionSelected = options[0];
        } else if (option2.isSelected()) {
            optionSelected = options[1];
        } else if (option3.isSelected()) {
            optionSelected = options[2];
        } else {
            optionSelected = options[3];
        }
        // para no poder confirmar sin seleccionar una opcion
        confirm.setDisable(false);
    }

    private void clearRadios() {
        option1.setSelected(false);
        option2.setSelected(false);
        option3.setSelected(false);
        option4.setSelected(false);
    }

    private void finished() {
        // setea false a la visibilidad de elementos del quiz
        //y true a los resultados
        win.setVisible(true);
        winBuenas.setVisible(true);
        winMalas.setVisible(true);
        winBuenas1.setText(String.valueOf(contBuenas));
        winBuenas1.setVisible(true);
        winMalas1.setText(String.valueOf(contMalas));
        winMalas1.setVisible(true);
        preguntaNo.setVisible(false);
        question.setVisible(false);
        option1.setVisible(false);
        option2.setVisible(false);
        option3.setVisible(false);
        option4.setVisible(false);
        confirm.setVisible(false);
        //Ingreso a la base de datos
        Hashtable<Integer, Object> values = new Hashtable<>();
        values.put(1, contBuenas);
        values.put(2, contMalas);
        values.put(3, Main.usuario.getEdad());
        conexion.insert(Quiz.class, values);
    }

}
