/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar lo que ocurre en la ventana preguntas
Ultima modificacion: 25/09/2019

 */
package org.tabunem.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Pregunta;

import java.awt.*;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.tabunem.system.Main;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.ResourceBundle;

public class PreguntasController implements Initializable {

    public static Pregunta pregunta;

    //Variables para FXML
    @FXML
    ListView preguntasAnteriores;
    @FXML
    TableView preguntasTable;
    @FXML
    TableColumn idColumn;
    @FXML
    TableColumn preguntaColumn;
    @FXML
    TableColumn votosColumn;
    @FXML
    Button responderButton;
    @FXML
    TextField ingresoPreguntas;

    // observable del que el listView obtiene sus elementos
    ObservableList<Pregunta> observable = FXCollections.observableArrayList();

    //Controlador que se encargara de obtener el ResulSet y añadirlo al observable
    public PreguntasController()throws Exception{
        ResultSet rs=conexion.selectAllOrdered(Pregunta.class, "votaciones", "WHERE respuestas = 0");
        while(rs.next()){
            if(rs.getInt("id")>10){
                observable.add(new Pregunta(rs.getInt("id"), rs.getString("descripcion"), rs.getString("opciones"), rs.getInt("votaciones"), rs.getInt("respuestas"), rs.getInt("idUsuario")));
            }
        }
    }

    //Metodo para inicializar el proceso cuando se cargue la vista
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        preguntasTable.setItems(observable);
        idColumn.setCellValueFactory(new PropertyValueFactory<Pregunta, Integer>("id"));
        preguntaColumn.setCellValueFactory(new PropertyValueFactory<Pregunta, String>("descripcion"));
        votosColumn.setCellValueFactory(new PropertyValueFactory<Pregunta, Integer>("votaciones"));
        responderButton.setDisable(true);
        if(Main.usuario.getClass().getSimpleName().equals("Especialista")) {
            responderButton.setVisible(true);
        } else {
            responderButton.setVisible(false);
        }
        //temp
        votosColumn.setVisible(false);
    }

    dbConnection conexion = new dbConnection(); //Conexion a base de datos

    //Metodo para regresar a la ventana principal al recibir el ActionEvent
    public void changeScreenButtonPushed(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Principal.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }

    //Metodo para obtener la pregunta e insertarla en la base de datos
    public void enviarPreguntas(ActionEvent event)throws Exception{
        //si el textField no esta vacio
        if(!ingresoPreguntas.getText().equals("")){
            //Crea el diccionario con los valores que se agregaran a la tabla Pregunta
            Hashtable<Integer, Object> values = new Hashtable<>();
            values.put(1, ingresoPreguntas.getText()); //pregunta
            values.put(2, ""); //opciones
            values.put(3, 0); // votos
            values.put(4, 0);//respuestas
            values.put(5, Main.usuario.getId()); //idUsuario
            //inserta la pregunta con el usuario logeado actualmente
            conexion.insert(Pregunta.class,values);
            //Vuelve a traer todas las preguntas, incluida la recien hecha
            ResultSet rs=conexion.selectAllOrdered(Pregunta.class, "votaciones", "WHERE respuestas = 0");
            observable.clear();
            while(rs.next()){
                if(rs.getInt("id")>10){
                    observable.add(new Pregunta(rs.getInt("id"), rs.getString("descripcion"), rs.getString("opciones"), rs.getInt("votaciones"), rs.getInt("respuestas"), rs.getInt("idUsuario")));
                    ingresoPreguntas.setText("");
                }
            }
        }

    }

    public void seleccionar() {
        if (preguntasTable.getSelectionModel().getSelectedIndex() > -1) {
            pregunta = (Pregunta)preguntasTable.getSelectionModel().getSelectedItem();
            responderButton.setDisable(false);
        }
    }

    //Metodo para regresar a la ventana principal al recibir el ActionEvent
    public void responder(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Respuesta.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }
}
