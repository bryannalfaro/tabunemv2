/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar la ventana de registro
Ultima modificacion: 05/09/2019

 */
package org.tabunem.controllers;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Especialista;
import org.tabunem.models.Usuario;
import org.tabunem.system.Main;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class RegistroController implements Initializable {

    dbConnection connection = new dbConnection();

    public void cambioPantalla(javafx.event.ActionEvent accion) throws IOException {
        Parent RegistroViewParent = FXMLLoader.load(getClass().getResource("../views/Login.fxml"));
        Scene RegistroViewScene = new Scene(RegistroViewParent);

        Stage window = (Stage)((Node)accion.getSource()).getScene().getWindow();
        window.setScene(RegistroViewScene);
        window.show();
    }

    @FXML
    private Button reg;
    @FXML
    private TextField user;
    @FXML
    private TextField pass;
    @FXML
    private TextField email;
    @FXML
    private TextField edad;

    @FXML
    public void clickUser(javafx.event.ActionEvent accion) {

    }

    @FXML
    public void clickPass(javafx.event.ActionEvent accion) {

    }

    @FXML
    public void clickEmail(javafx.event.ActionEvent accion) {

    }

    @FXML
    public void clickEdad(javafx.event.ActionEvent accion) {

    }

    @FXML
    public void clickReg(javafx.event.ActionEvent accion) throws IOException, InvalidKeyException, NoSuchAlgorithmException, SQLException {
        // obtiene los valores de los textfields y los ingresa en el diccionario que se enviara para ingresar
        Hashtable<Integer, Object> values = new Hashtable<>();
        values.put(1, user.getText()); //nombre
        values.put(2, Integer.valueOf(edad.getText())); //edad
        values.put(3, email.getText()); //email
        values.put(4, encryptPassword(pass.getText())); //Encripta la contrasena
        values.put(5, ""); // Acerca de mi vacio
        connection.insert(Usuario.class, values); //ingresa a la base de datos
        //Llama al elemento que se creo para obtener su id
        Hashtable<Object, String> usuario = new Hashtable<Object, String>();
        usuario.put(user.getText(),"nombre");
        usuario.put(encryptPassword(pass.getText()),"pass");
        ResultSet rs = connection.selectByParameters(Usuario.class, usuario);
        while (rs.next()) {
            // setea el usuario obtenido a la variable estatica en el Main
            ResultSet rs2 = connection.selectById(Especialista.class, rs.getInt("id"));
            if (rs2.next()) {
                Especialista especialista = new Especialista(rs.getInt("id"), rs.getString("nombre"), rs.getInt("edad"), rs.getString("userName"), rs.getString("pass"), rs.getString("acercaDeMi"),rs2.getString("especialidad"),rs2.getInt("respondidas"));
            } else {
                Usuario usuarioToMain = new Usuario(rs.getInt("id"), rs.getString("nombre"), rs.getInt("edad"), rs.getString("userName"), rs.getString("pass"), rs.getString("acercaDeMi"));
                Main.usuario = usuarioToMain;
            }
        }
        changeScreenButtonPushedMenu(accion);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    //Metodo para llamar a la ventana principal
    public void changeScreenButtonPushedMenu(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Principal.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }

    private String encryptPassword(String pass) throws NoSuchAlgorithmException, InvalidKeyException {
        String passEncrypt;
        //convierte al hash de una via (no se puede desencriptar) HmacSHA256 por lo tanto usa una llave
        Mac hmacsha256 = Mac.getInstance("HmacSHA256");
        //Esta llave puede ser cualquiera XD, se inicia el hash con los parametros que deseados
        hmacsha256.init(new SecretKeySpec("llabeXD".getBytes(StandardCharsets.UTF_8), "HmacSHA256"));
        // ingresa al algoritmo la contrasena normal
        byte[] hash = hmacsha256.doFinal(pass.getBytes(StandardCharsets.UTF_8));
        // para construir el nuevo string de contrasena encriptada
        StringBuffer sb = new StringBuffer();
        // ingresa al stringBuffer cada uno de los char de la contrasena encriptada
        for (byte b : hash) {
            /* Convertir byte a hexadecimal
            >% de conversión [flags] [width]
            > Bandera ‘0’ – El resultado se rellenará con cero
            > Ancho 2
            > Conversión ‘X’: el resultado se formatea como un entero hexadecimal, en mayúsculas
            via> https://codeday.me/es/qa/20181211/19840.html*/
            sb.append(String.format("%02x", b));
        }
        passEncrypt = sb.toString();
        return passEncrypt;
    }
}

