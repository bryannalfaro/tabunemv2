/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar el menu primero
Ultima modificacion: 05/09/2019

 */
package org.tabunem.controllers;


import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuPrimeroController {
    //Funcion para llamar a la ventana login
    public void changeScreenButtonPushedLogin(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Login.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }





}
