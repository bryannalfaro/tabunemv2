/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar lo que ocurre en la ventana respuesta
esta ventana sale solo a partir de la ventana de preguntas, despues de seleccionar
que pregunta se quiere responder
Ultima modificacion: 9/11/2019

 */
package org.tabunem.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Pregunta;

import java.awt.*;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.tabunem.models.Respuesta;
import org.tabunem.system.Main;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.ResourceBundle;

public class RespuestaController implements Initializable {
    //Variables para FXML
    @FXML
    Button cancelarButton;
    @FXML
    Label preguntaLabel;
    @FXML
    Button responderButton;
    @FXML
    TextField simpleTextField;
    @FXML
    TextArea explicacionTextArea;
    @FXML
    TextField opcion1TextField;
    @FXML
    TextField opcion2TextField;
    @FXML
    TextField opcion3TextField;

    //Metodo para inicializar el proceso cuando se cargue la vista
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        preguntaLabel.setText(PreguntasController.pregunta.getDescripcion());
        responderButton.setDisable(true);
    }

    dbConnection conexion = new dbConnection(); //Conexion a base de datos

    //Metodo para regresar a la ventana preguntas al recibir el ActionEvent
    public void cancelar(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent respuestaViewParent = FXMLLoader.load(getClass().getResource("../views/Preguntas.fxml"));
        Scene respuestaViewScene = new Scene(respuestaViewParent,850,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(respuestaViewScene);
        window.show();
    }

    public void comprobarCampos() {
        if (!simpleTextField.getText().equals("") && !explicacionTextArea.getText().equals("")
        && !opcion1TextField.getText().equals("") && !opcion2TextField.getText().equals("") && !opcion3TextField.getText().equals("")) {
            responderButton.setDisable(false);
        }
    }

    public void responder(ActionEvent event) throws Exception {
        //Crea el diccionario con los valores que se agregaran a la tabla Respuesta
        Hashtable<Integer, Object> values = new Hashtable<>();
        values.put(1, simpleTextField.getText());
        values.put(2, explicacionTextArea.getText());
        values.put(3, PreguntasController.pregunta.getId());
        values.put(4, Main.usuario.getId());
        //inserta la respuesta con el usuario logeado actualmente
        try {
            conexion.insert(Respuesta.class,values);
            Hashtable<String, Object> values2 = new Hashtable<>();
            values2.put("opciones", "'"+simpleTextField.getText()+"/"+opcion1TextField.getText()+"/"+opcion2TextField.getText()+"/"+opcion3TextField.getText()+"'");
            values2.put("respuestas", "respuestas + 1");
            conexion.update(Pregunta.class, values2, PreguntasController.pregunta.getId());
            cancelar(event);
        } catch (Exception e) {
            System.out.println("no se ha podido agregar la respuesta ni acutalizar la pregunta con sus opciones");
        }
    }

}
