package org.tabunem.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Quiz;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class EstadisticasController implements Initializable {

    @FXML
    private BarChart<String, Integer> barChart;

    @FXML
    private CategoryAxis xAxis;

    @FXML
    private Label conteo;

    private dbConnection conexion = new dbConnection();
    private ObservableList<String> ejeXList = FXCollections.observableArrayList();
    private MenuPrimeroController menuPrimeroController;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ResultSet rs = conexion.selectAll(Quiz.class);
        int cont = 0;
        while (true) {
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            cont++;
        }
        conteo.setText(String.valueOf(cont));
    }

    public void Estadisticas(ActionEvent actionEvent) throws Exception {
        String[] ejeX = {"Correctas", "Incorrectas"};
        ejeXList.addAll(ejeX);
        xAxis.setCategories(ejeXList);
        int[] ejeY = new int[2];
        ResultSet rs = conexion.selectAll(Quiz.class);
        int cont = 0;
        while (rs.next()) {
            // Agrega las correctas e incorrectas alo array
            ejeY[0] += rs.getInt(2);
            ejeY[1] += rs.getInt(3);
            cont++;
        }
        XYChart.Series<String, Integer> series = new XYChart.Series<>();
        // Create a XYChart.Data object for each month. Add it to the series.
        for (int i = 0; i < ejeY.length; i++) {
            series.getData().add(new XYChart.Data<>(ejeXList.get(i), ejeY[i]));
        }
        barChart.getData().add(series);
    }

    //Metodo para regresar a la ventana principal al recibir el ActionEvent
    public void changeScreenButtonPushed(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Principal.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }
}