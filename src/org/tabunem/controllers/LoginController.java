/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar  la ventana de login
Ultima modificacion: 05/09/2019

 */
package org.tabunem.controllers;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Especialista;
import org.tabunem.models.Usuario;
import org.tabunem.system.Main;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;


public class LoginController implements Initializable {
    //variable para conectar a base de datos
    dbConnection connection = new dbConnection();
    //Variables de FXML
    @FXML
    private Button log;
    @FXML
    private TextField user;
    @FXML
    private TextField pass;
    @FXML
    private Button reg;
    @FXML
    private AnchorPane pane;
    @FXML
    private Label ingresos;

    public LoginController() {

    }

    @FXML
    public void clickUser(javafx.event.ActionEvent accion) {

    }

    @FXML
    public void clickPass (javafx.event.ActionEvent accion) {

    }

    @FXML
    public void clickLog(javafx.event.ActionEvent accion) throws IOException, SQLException, InvalidKeyException, NoSuchAlgorithmException {
        // construye el diccionario con los valores de usuario y contrasena
        // para traer el usuario con esos parametros
        Hashtable<Object, String> values = new Hashtable<>();
        values.put(user.getText(), "nombre");
        values.put(encryptPassword(pass.getText()), "pass");
        // obtiene el usuario
        ResultSet rs = connection.selectByParameters(Usuario.class, values);
        //si el usuario existe
        //si no no existe el .next()
        if (rs.next()) {
            // setea el usuario obtenido a la variable estatica en el Main
            ResultSet rs2 = connection.selectById(Especialista.class, rs.getInt("id"));
            if (rs2.next()) {
                Especialista especialista = new Especialista(rs.getInt("id"), rs.getString("nombre"), rs.getInt("edad"), rs.getString("userName"), rs.getString("pass"), rs.getString("acercaDeMi"),rs2.getString("especialidad"),rs2.getInt("respondidas"));
                Main.usuario = especialista;
            } else {
                Usuario usuario = new Usuario(rs.getInt("id"), rs.getString("nombre"), rs.getInt("edad"), rs.getString("userName"), rs.getString("pass"), rs.getString("acercaDeMi"));
                Main.usuario = usuario;
            }
            changeScreenButtonPushedMenu(accion);
        }
        else{
            //Mensaje de error si se ingresa mal
            ingresos.setText("Error, inténtalo de nuevo");
        }
    }

    //Metodo para llamar a la ventana registro
    public void cambioReg(javafx.event.ActionEvent accion) throws IOException {
        Parent RegistroViewParent = FXMLLoader.load(getClass().getResource("../views/Registro.fxml"));
        Scene RegistroViewScene = new Scene(RegistroViewParent,800,400);

        Stage window = (Stage) ((Node) accion.getSource()).getScene().getWindow();
        window.setScene(RegistroViewScene);
        window.show();

    }

    //Metodo para llamar a la ventana principal
    public void changeScreenButtonPushedMenu(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("../views/Principal.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent,800,400);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    //Metodo para encriptar la contrasena
    //Es necesario encryptar la contrasena en el logeo ya que se compara con la de la base de datos
    //que tambien esta encriptada
    private String encryptPassword(String pass) throws NoSuchAlgorithmException, InvalidKeyException {
        String passEncrypt;
        Mac hmacsha256 = Mac.getInstance("HmacSHA256");
        hmacsha256.init(new SecretKeySpec("llabeXD".getBytes(StandardCharsets.UTF_8), "HmacSHA256"));
        byte[] hash = hmacsha256.doFinal(pass.getBytes(StandardCharsets.UTF_8));
        StringBuffer sb = new StringBuffer();
        for (byte b : hash) {
            /* Convertir byte a hexadecimal
            >% de conversión [flags] [width]
            > Bandera ‘0’ – El resultado se rellenará con cero
            > Ancho 2
            > Conversión ‘X’: el resultado se formatea como un entero hexadecimal, en mayúsculas
            via> https://codeday.me/es/qa/20181211/19840.html*/
            sb.append(String.format("%02x", b));
        }
        passEncrypt = sb.toString();
        return passEncrypt;
    }
}

