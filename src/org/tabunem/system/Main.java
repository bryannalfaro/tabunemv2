/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Programa principal para correr el programa TABUNEM y llamar a las vistas
Ultima modificacion: 25/09/2019

 */
package org.tabunem.system;

import com.mysql.cj.jdbc.JdbcConnection;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.tabunem.database.dbConnection;
import org.tabunem.models.Pregunta;
import org.tabunem.models.Respuesta;
import org.tabunem.models.Usuario;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

public class Main extends Application {
    //Metodo para establecer la escena
    public static Usuario usuario;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../views/Menu_Primero.fxml"));
        primaryStage.setTitle("TABUNEM");
        primaryStage.setScene(new Scene(root, 800, 400));
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args); }
}

