import matplotlib.pyplot as plt
import numpy as np

print("Corriendo Python")

def graficaPersonal(gen, emb, ma, its):
    x = ['General de Temas', 'Embarazos','Metodos A.', 'ITS']
    y = [gen, emb, ma, its]
    plt.bar(np.arange(4), y)
    plt.xticks(np.arange(4), x)
    plt.title("Gráfica de Estadísticas Personales")
    plt.ylabel('% de Correctas')
    plt.show() #Mostrar la gráfica