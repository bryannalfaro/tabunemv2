DROP DATABASE IF EXISTS tabunem;
CREATE DATABASE tabunem;
USE tabunem;

CREATE TABLE Usuario(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(256) NOT NULL UNIQUE,
    edad INT NOT NULL,
    userName VARCHAR(256) NOT NULL UNIQUE,
    pass VARCHAR(256) NOT NULL,
    acercaDeMi VARCHAR(4096) NOT NULL
);

CREATE TABLE Especialista(
    id INT NOT NULL PRIMARY KEY,
    especialidad VARCHAR(256) NOT NULL,
    respondidas INT NOT NULL,
    FOREIGN KEY Especialista_Usuario (id) REFERENCES Usuario (id)
);

CREATE TABLE Pregunta(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    descripcion VARCHAR(4096) NOT NULL,
    opciones VARCHAR(4096) NOT NULL,
    votaciones INT NOT NULL,
    respuestas INT NOT NULL DEFAULT 0,
    idUsuario INT NOT NULL,
    CONSTRAINT FOREIGN KEY Pregunta_Usuario (idUsuario) REFERENCES Usuario (id)
);

CREATE TABLE Respuesta(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    respuesta VARCHAR(4096) NOT NULL,
    explicacion VARCHAR(4096) NOT NULL,
    idPregunta INT NOT NULL,
    idUsuario INT NOT NULL,
    CONSTRAINT FOREIGN KEY Respuesta_Pregunta (idPregunta) REFERENCES Pregunta (id),
    CONSTRAINT FOREIGN KEY Respuesta_Usuario (idUsuario) REFERENCES Usuario (id)
);

CREATE TABLE ImagenesInformativa(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    url VARCHAR(256) NOT NULL
);

CREATE TABLE Mito(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    descripcion VARCHAR(2048) NOT NULL
);

CREATE TABLE Quiz(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	correctas INT NOT NULL,
	incorrectas INT NOT NULL,
	edad INT NOT NULL 
);
