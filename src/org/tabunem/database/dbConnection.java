/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de controlar lo que ocurre en la ventana preguntas
Ultima modificacion: 25/09/2019

 */
package org.tabunem.database;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class dbConnection {
    // String connection, integracion con el driver y parametros para la conexion
    private String url = "jdbc:mysql://localhost:3306/tabunem?autoReconnect=true&useSSL=false&serverTimezone=UTC";
    private String user = "root";
    private String password = "";
    private Connection connection;
    private Statement statement; // Para ejecutar los querys

    public dbConnection() {
        try {
            // "Instanciar" la conexion
            connection = DriverManager.getConnection(url, user, password);
            if (connection != null) {
                System.out.println("Connection to database " + connection.getCatalog());
            } else {
                System.out.println("Connection to database failed!");
            }
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
        Los metodos CRUD funcionan mediante a una convencion existente entre la base de datos
        y los modelos en Java. Estos metodos son dinamicos, aceptando una clase modelo para saber
        a que tablas y parametros llamar desde la base de datos
    */

    public ResultSet selectAll(Class<?> clazz) {
        ResultSet resultSet; // Objeto que contiene la informacion que viene de la base de datos
        try {
            String table = clazz.getSimpleName(); // obtiene el nombre de la clase
            resultSet = statement.executeQuery("SELECT * FROM " + table); // ejecuta el query
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet selectAllOrdered(Class<?> clazz, String order) {
        ResultSet resultSet; // Objeto que contiene la informacion que viene de la base de datos
        try {
            String table = clazz.getSimpleName(); // obtiene el nombre de la clase
            resultSet = statement.executeQuery("SELECT * FROM " + table + " ORDER BY " + order); // ejecuta el query
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet selectAllOrdered(Class<?> clazz, String order, String otherParameter) {
        ResultSet resultSet; // Objeto que contiene la informacion que viene de la base de datos
        try {
            String table = clazz.getSimpleName(); // obtiene el nombre de la clase
            resultSet = statement.executeQuery("SELECT * FROM " + table + " " + otherParameter + " ORDER BY " + order); // ejecuta el query
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet selectById(Class<?> clazz, int id) {
        ResultSet resultSet;
        try {
            String table = clazz.getSimpleName();
            // selecciona solo un registro por medio de su id en la base de datos
            //PreparedStatement permite ingresar los valores aparte y no de una vez en el queryString.
            //Los valores que se ingresaran se muestran en el query con el simbolo de interrogacion ?
            PreparedStatement ps = connection.prepareCall("SELECT * FROM " + table + " WHERE id = ?");
            //Los valores que se declaran en el query se setean despues al PreparedStatement
            //indicando su orden y el valor
            ps.setInt(1, id);
            resultSet = ps.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet selectByParameters(Class<?> clazz, Hashtable<Object, String> values) {
        // el diccionario que recibe obtiene como objeto el valor que se quiere condicionar
        //y el string es el nombre de la columna de ese valor
        ResultSet resultSet;
        try {
            String table = clazz.getSimpleName();
            String query = "SELECT * FROM " + table + " WHERE";
            int cont = 1;
            //construye el query con la cantidad de condiciones (tabla y valor) que se ingresan
            for (Map.Entry<Object, String> entry : values.entrySet()) {
                query += " " + entry.getValue() + " = " + "?";
                if (cont != values.size()) {
                    query += " AND";
                }
                cont++;
            }
            PreparedStatement ps = connection.prepareCall(query);
            cont = 1;
            //Ingresa los valores en el PreparedStatement
            //Si son int los pone como int; si son String como String
            //puede que falten aqui como por ejemplo el Double... agregar si da error
            for (Map.Entry<Object, String> entry : values.entrySet()) {
                if (entry.getKey().getClass().equals(Integer.class)) {
                    ps.setInt(cont, (Integer) entry.getKey());
                } else if (entry.getKey().getClass().equals(String.class)) {
                    ps.setString(cont, (String) entry.getKey());
                }
                cont++;
            }
            System.out.println(query);
            resultSet = ps.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insert(Class<?> clazz, Hashtable<Integer, Object> values) {
        // el diccionario que recibe tiene como objeto el valor que se ingresara a la tabla
        // y el integer es el orden en el que se ingresa
        try {
            String table = clazz.getSimpleName();
            String query = "INSERT INTO " + table + " (";
            // Contruye el insert de los valores que se ingresaran
            //mediante los parametros del modelo en Java
            for (Field field : clazz.getDeclaredFields()) {
                if (!field.getName().equals("id")) {
                    query += (field.getName() + ",");
                }
            }
            // quita la coma al ultimo parametro
            query = query.substring(0, query.length()-1) + ") VALUES (";
            // ingresa los ? en el query
            for (Field field : clazz.getDeclaredFields()) {
                query += "?,";
            }
            query = query.substring(0, query.length()-3) + ")";
            System.out.println(query);
            PreparedStatement ps = connection.prepareCall(query);
            //Ingresa los valores en el PreparedStatement
            //Si son int los pone como int; si son String como String
            //TODO: puede que falten aqui como por ejemplo el Double... agregar si da error
            for (Map.Entry<Integer, Object> entry : values.entrySet()) {
                if (entry.getValue().getClass().equals(Integer.class)) {
                    ps.setInt(entry.getKey(), (Integer) entry.getValue());
                } else if (entry.getValue().getClass().equals(String.class)) {
                    ps.setString(entry.getKey(), (String) entry.getValue());
                }
            }
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(Class<?> clazz, Hashtable<String, Object> values, int id) {
        //Este metodo recibe como valores, como llave el nombre especifico de la columna que se modificara
        //y como valor, el valor por el que se reemplazara
        //recuerde que como es un update no es necesario actualizar todos los campos de la tabla
        try {
            String table = clazz.getSimpleName();
            String query = "UPDATE " + table + " SET ";
            // Contruye el update de los valores que se cambiaran
            //mediante los valores que vienen de la variable values
            for (Map.Entry<String, Object> entry : values.entrySet()) {
                query += entry.getKey() + " = " + entry.getValue() + ", ";
            }
            query = query.substring(0, query.length()-2);
            query += " WHERE id = " + id;
            System.out.println(query);
            PreparedStatement ps = connection.prepareCall(query);
            ps.execute(); //ejecuta el query
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet selectJoin(ArrayList<Class<?>> tables) {
        // La primera tabla en el array es la principal (si importa el orden)
        ResultSet resultSet;
        try {
            //llama al metodo que construye el query de joins
            String query = buildQueryJoin(tables);
            System.out.println(query);
            PreparedStatement ps = connection.prepareCall(query);
            resultSet = ps.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet selectJoinOrdered(ArrayList<Class<?>> tables, String order) {
        ResultSet resultSet;
        try {
            //llama al metodo que construye el query de joins
            String query = buildQueryJoin(tables);
            //Agrega la sentencia para ordenar
            query += "ORDER BY " + order;
            System.out.println(query);
            PreparedStatement ps = connection.prepareCall(query);
            resultSet = ps.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet selectJoinOrdered(ArrayList<Class<?>> tables, String order, String otherParameter) {
        ResultSet resultSet;
        try {
            //llama al metodo que construye el query de joins
            String query = buildQueryJoin(tables);
            //Agrega la sentencia para ordenar y algun otro parametro como WHERE
            //esta otra condicion se tiene que especificar directamente cuando se llama al metodo
            query += " ORDER BY " + order + " " + otherParameter;
            System.out.println(query);
            PreparedStatement ps = connection.prepareCall(query);
            resultSet = ps.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String buildQueryJoin(ArrayList<Class<?>> tables) {
        String query = "SELECT";
        // Hace distinguir las columnas de las tablas que se hacen join poniendole un alias
        for (Class<?> clazz : tables) {
            for (Field field : clazz.getDeclaredFields()) {
                query += " " + clazz.getSimpleName() + "." + field.getName() + " AS ";
                query += "\"" + clazz.getSimpleName() + "." + field.getName()+ "\",";
            }
        }
        //agrega la tabla principal
        query = query.substring(0, query.length()-1) + " FROM " + tables.get(0).getSimpleName();
        //Inserta los parametros en los que se relacionan las tablas de esta manera
        //la referencia de la segunda tabla en la primera es igual al id de la segunda tabla
        //y asi sucesivamente
        for (int i = 1; i < tables.size(); i++) {
            query += " JOIN " + tables.get(i).getSimpleName() + " ON ";
            query += tables.get(i - 1).getSimpleName() + ".id" + tables.get(i).getSimpleName() + " = " + tables.get(i).getSimpleName() + ".id";
        }
        return query;
    }

}
