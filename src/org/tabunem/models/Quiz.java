package org.tabunem.models;

public class Quiz {
    private int id;
    private int correctas;
    private int incorrectas;
    private int edad;

    public Quiz() {}

    public Quiz(int id, int correctas, int incorrectas, int edad) {
        this.id = id;
        this.correctas = correctas;
        this.incorrectas = incorrectas;
        this.edad = edad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCorrectas() { return correctas; }

    public void setCorrectas(int correctas) { this.correctas = correctas; }

    public int getIncorrectas() { return incorrectas; }

    public void setIncorrectas(int incorrectas) { this.incorrectas = incorrectas; }

    public int getEdad() { return edad; }

    public void setEdad(int edad) { this.edad = edad; }
}
