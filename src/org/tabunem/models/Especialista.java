package org.tabunem.models;

public class Especialista extends Usuario {
    private String especialidad;
    private Integer respondidas;

    public Especialista(String nombre, Integer edad, String userName, String pass, String acercaDeMi, String especialidad, Integer respondidas) {
        super(nombre, edad, userName, pass, acercaDeMi);
        this.especialidad = especialidad;
        this.respondidas = respondidas;
    }

    public Especialista(Integer id, String nombre, Integer edad, String userName, String pass, String acercaDeMi, String especialidad, Integer respondidas) {
        super(id, nombre, edad, userName, pass, acercaDeMi);
        this.especialidad = especialidad;
        this.respondidas = respondidas;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public Integer getRespondidas() {
        return respondidas;
    }

    public void setRespondidas(Integer respondidas) {
        this.respondidas = respondidas;
    }
}
