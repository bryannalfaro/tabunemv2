package org.tabunem.models;

public class Usuario {
    private Integer id;
    private String nombre;
    private Integer edad;
    private String userName;
    private String pass;
    private String acercaDeMi;

    public Usuario(Integer id, String nombre, Integer edad, String userName, String pass, String acercaDeMi) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.userName = userName;
        this.pass = pass;
        this.acercaDeMi = acercaDeMi;
    }

    public Usuario(String nombre, Integer edad, String userName, String pass, String acercaDeMi) {
        this.nombre = nombre;
        this.edad = edad;
        this.userName = userName;
        this.pass = pass;
        this.acercaDeMi = acercaDeMi;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public String getUserName() {
        return userName;
    }

    public String getPass() {
        return pass;
    }

    public String getAcercaDeMi() {
        return acercaDeMi;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setAcercaDeMi(String acercaDeMi) {
        this.acercaDeMi = acercaDeMi;
    }
}
