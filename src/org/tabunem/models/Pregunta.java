package org.tabunem.models;

public class Pregunta {
    private int id;
    private String descripcion;
    private String opciones;
    private int votaciones;
    private int respuestas;
    private int idUsuario;

    public Pregunta() {}

    public Pregunta(int id, String descripcion, String opciones, int votaciones, int respuestas, int idUsuario) {
        this.id = id;
        this.descripcion = descripcion;
        this.opciones = opciones;
        this.votaciones = votaciones;
        this.respuestas = respuestas;
        this.idUsuario = idUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getOpciones() {
        return opciones;
    }

    public void setOpciones(String opciones) {
        this.opciones = opciones;
    }

    public int getVotaciones() {
        return votaciones;
    }

    public void setVotaciones(int votaciones) {
        this.votaciones = votaciones;
    }

    public int getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(int respuestas) {
        this.respuestas = respuestas;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
