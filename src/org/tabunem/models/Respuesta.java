/*
Universidad del valle de Guatemala
Integrantes:
Bryann Alfaro
Cecilia Solares
Diego Arredondo
Julio Herrera
Sofia Rueda
Descripcion: Clase que se encarga de ser el modelo de Respuesta
Ultima modificacion: 25/09/2019

 */
package org.tabunem.models;

public class Respuesta {
    //Creacion de variables
    private int id;
    private String respuesta;
    private String explicacion;
    private int idPregunta;
    private int idUsuario;

    public Respuesta() {}

    public Respuesta(int id, String respuesta, String explicacion, int idPregunta, int idUsuario) {
        this.id = id;
        this.respuesta = respuesta;
        this.explicacion = explicacion;
        this.idPregunta = idPregunta;
        this.idUsuario = idUsuario;
    }
    //Metodos getters y setters
    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getExplicacion() {
        return explicacion;
    }

    public void setExplicacion(String explicacion) {
        this.explicacion = explicacion;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}


